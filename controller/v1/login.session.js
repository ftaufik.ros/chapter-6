const { superUsers } = require('../../data/superUser')

exports.loginPage = async function(req, res) {
    res.render('login/login', {
        session: req.session
    });
}

exports.auth = function(req, res) {
    try {
        let message = {
            type: 'error',
            message: 'Login failed, username/password does not match'
        };

        let email = req.body.email;
        let password = req.body.password;
        
        let user = superUsers.find(u => u.email === email);
        
        if (user && user.password === password){
            req.session.userId = user.userId
            res.redirect('/');
        } else {
            res.render('login/login', {
                message: message
            });
        };
    } catch (error) {
        res.render('login/login', {
            error: error
        });
    }
}

exports.logout = function(req, res) {
    if (req.session.userId) {
        req.session.destroy();
        res.redirect('/');
    }
}