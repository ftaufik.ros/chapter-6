const { User, userGameHistories } = require("../../models");

exports.createHistoryForm = async function(req, res) {
    if (req.session.userId) {
        return res.redirect('/');
    };

    User.findByPk(req.params.id, {
        include: userGameHistories
    })
    .then(users => {
        res.render('user-game/user-history/createHistory', {
            session: req.session,
            users: users
        });
    });
}

exports.updateHistoryForm = async function(req, res) {
    if (req.session.userId) {
        return res.redirect('/');
    };
    
     userGameHistories.findByPk(req.params.id)
    .then(history => {
        res.render('user-game/user-history/editHistory', {
            session: req.session,
            history: history
        });
    });
}

exports.createHistory = async function(req, res) {
    try {
        let history = await userGameHistories.create({
            userId: req.body.id,
            gameName: req.body.gameName,
            playTime: req.body.playTime
        });
        
        res.redirect(`/show/${req.body.id}`);
    } catch (error) {
        res.render('user-game/user-history/createHistory', {
            error: error
        });
    }
}

exports.editHistory = async function(req, res) {
    try {
        let history = await userGameHistories.findByPk(req.body.id);
        history.gameName = req.body.gameName ? req.body.gameName : history.gameName;
        history.playTime = req.body.playTime ? req.body.playTime : history.playTime;
        await history.save();

        res.redirect(`/show/${req.body.userId}`);
    } catch (error) {
        res.render('user-game/user-history/editHistory', {
            error: error
        });
    }
}

exports.destroyHistory = async function(req, res) {
    try {
        const count = await userGameHistories.destroy({
            where: { id: req.params.id }
        });

        if (count > 0) {
            return res.redirect('/');
        } else {
            return res.redirect('/');
        }
    } catch (err) {
        return res.redirect('/');
    };
}