const { User, userBiodata } = require("../../models");

exports.editBiodataForm = async function(req, res) {
    if (req.session.userId) {
        return res.redirect('/');
    };
    
    User.findByPk(req.params.id, {
        include: userBiodata
    })
    .then(bio => {
        res.render('user-game/user-biodata/editBiodata', {
            session: req.session,
            bio: bio
        });
    });
}

exports.editBiodata = async function(req, res) {
    try {
        let biodataUser = await userBiodata.findByPk(req.body.id);
        biodataUser.fullName = req.body.fullName ? req.body.fullName : biodataUser.fullName;
        biodataUser.age = req.body.age ? req.body.age : biodataUser.age;
        biodataUser.country = req.body.country ? req.body.country : biodataUser.country;
        await biodataUser.save();

        res.redirect(`/show/${req.body.userId}`);
    } catch (error) {
        res.render('user-game/user-biodata/editBiodata', {
            error: error
        });
    }
}