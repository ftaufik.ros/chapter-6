const { User, userBiodata, userGameHistories } = require("../../models");

exports.index = async function(req, res) {
    await User.findAll({
        where: {},
        order: [
            ['updatedAt', 'desc'],
            // ['createdAt', 'desc']
        ],
        include: userBiodata
    })
    .then(users => {
        res.render('user-game/index', {
            session: req.session,
            users: users
        });
    });
}

exports.createForm = async function(req, res) {
    if (req.session.userId) {
        return res.redirect('/');
    };
    
    User.findByPk(req.params.id, {
        include: userBiodata
    })
    .then(users => {
        res.render('user-game/create', {
            session: req.session,
            users: users
        });
    });
}

exports.updateForm = async function(req, res) {
    if (req.session.userId) {
        return res.redirect('/');
    };

     User.findByPk(req.params.id, {
        include: userBiodata
    })
    .then(users => {
        res.render('user-game/edit', {
            session: req.session,
            users: users
        });
    });
}

exports.show = async function (req, res) {
    if (req.session.userId) {
        return res.redirect('/');
    };
    
    User.findByPk(req.params.id, {
        include: [{
            model : userBiodata
        },
        {
            model : userGameHistories
        }]
    })
    .then(users => {
        res.render('user-game/show', {
            session: req.session,
            users: users
        });
    });
}

exports.create = async function(req, res) {
    try {
        let user = await User.create({
            userName: req.body.userName,
            email: req.body.email,
            password: req.body.password,
        });
        let biodata = await userBiodata.create({
            userId: user.id,
            fullName: req.body.fullName,
            age: req.body.age,
            country: req.body.country,
        });
        res.redirect('/');
    } catch (error) {
        res.render('user-game/create', {
            error: error
        });
    }
}

exports.edit = async function(req, res) {
    try {
        let users = await User.findByPk(req.body.id);
        users.userName = req.body.userName ? req.body.userName : users.userName;
        users.email = req.body.email ? req.body.email : users.email;
        users.password = req.body.password ? req.body.password : users.password;
        await users.save();

        res.redirect('/');
    } catch (error) {
        res.render('user-game/edit', {
            error: error
        });
    }
}

exports.destroy = async function(req, res) {
    try {
        const count = await User.destroy({
            where: { id: req.params.id }
        });

        if (count > 0) {
            return res.redirect('/');
        } else {
            return res.redirect('/');
        }
    } catch (err) {
        return res.redirect('/');
    };
}
