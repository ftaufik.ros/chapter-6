const express = require("express");
const bodyParser = require("body-parser");
const sessionHandler = require("express-session");
const cookieParser = require("cookie-parser");


const { 
    loginPage, 
    logout, 
    auth 
} = require("./controller/v1/login.session");

const { 
    index, 
    createForm, 
    updateForm, 
    destroy, 
    create, 
    edit, 
    show 
} = require("./controller/v1/user.game");

const { 
    editBiodata, 
    editBiodataForm 
} = require("./controller/v1/user.biodata");

const { 
    createHistoryForm, 
    updateHistoryForm, 
    editHistory, 
    createHistory, 
    destroyHistory 
} = require("./controller/v1/user.game.history");




const app = express();

const urlEncoded = bodyParser.urlencoded({ extended: false });

const oneDay = 1000 * 60 * 60 * 24;

app.use(sessionHandler({
    secret: "secret",
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
}));

app.use(cookieParser());


const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

// -=-=-=-=-=-= VIEWS =-=-=-=-=-=-
app.get("/login", loginPage);
app.get("/", index);
app.get("/create", createForm);
app.get("/edit/:id", updateForm);
app.get("/show/:id", show);
app.get("/biodata/edit/:id", editBiodataForm);
app.get("/game-history/create/:id", createHistoryForm);
app.get("/game-history/edit/:id", updateHistoryForm);



// -=-=-=-=-=-=-= API =-=-=-=-=-=-=-

// Login
app.post("/api/login", urlEncoded, auth);

// Logut
app.get("/api/logout", logout);

// create user
app.post("/api/user/create", urlEncoded, create);

// edit user
app.post("/api/user/edit", urlEncoded, edit);

// edit biodata user
app.post("/api/user/biodata/edit", urlEncoded, editBiodata);

// edit game history user
app.post("/api/user/game-history/edit", urlEncoded, editHistory);

// create game history user
app.post("/api/user/game-history/create", urlEncoded, createHistory);

// hapus user
app.get("/api/user/delete/:id", destroy);

// hapus game history
app.get("/api/user/game-history/delete/:id", destroyHistory);



app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
});
