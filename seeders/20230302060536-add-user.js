'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Users', [
    {
      userName: 'Test1',
      email: 'test1@mail.com',
      password: 'tes1',
      isVerified: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      userName: 'Test2',
      email: 'test2@mail.com',
      password: 'tes2',
      isVerified: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      userName: 'Test3',
      email: 'test3@mail.com',
      password: 'tes3',
      isVerified: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};
